<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 align="center" tabindex="-1" class="heading-element" dir="auto">
	<a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/title.png"><img width="400" alt="相对论" src="https://github.com/relativty/Relativty/raw/master/ressources/img/title.png" style="max-width: 100%;"></a>
</h1><a id="" class="anchor" aria-label="永久链接：" href="#"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 align="center" tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
	支持 SteamVR 的开源 VR 头盔。
</font></font></h3><a id="user-content-open-source-vr-headset-with-steamvr-support" class="anchor" aria-label="永久链接：支持 SteamVR 的开源 VR 头盔。
" href="#open-source-vr-headset-with-steamvr-support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto">
	<strong>
		<a href="https://relativty.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网站</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
		•
		</font></font><a href="https://discord.gg/jARCsVb" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不和谐</font></font></a>
	</strong>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我是</font></font><a href="https://twitter.com/maxim_xyz?lang=en" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Maxim xyz</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，当我最好的朋友 Gabriel Combe 15 岁时，我们制作了自己的 VR 头盔，因为我们买不起。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5 年后：这款头盔成为 Relativty。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完全开源——</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">硬件</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">软件</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">固件</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Steam VR</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持。</font></font></li>
<li><font style="vertical-align: inherit;"><strong><font style="vertical-align: inherit;">以120FPS</font></strong><font style="vertical-align: inherit;">原生显示</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2K</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分辨率</font><font style="vertical-align: inherit;">。</font></font><strong><font style="vertical-align: inherit;"></font></strong><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与Arduino</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">兼容</font><font style="vertical-align: inherit;">。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实验性</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">身体追踪</font></font></strong></li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此存储库可作为构建指南，如需了解有关头盔的更多信息，请访问</font></font><a href="https://relativty.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty.com</font></font></a></strong></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为黑客而生</font></font></h2><a id="user-content-made-for-hackers" class="anchor" aria-label="永久链接：为黑客而生" href="#made-for-hackers"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">相对论</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不是消费品</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">我们在我的卧室里用烙铁和 3D 打印机制作了 Relativty，我们希望您也能这样做：</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自己构建它</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty 的基本原理是一款 3 自由度 (3-DoF) VR 头盔，没有内置控制器支持，旨在与 SteamVR 配合使用。</font><font style="vertical-align: inherit;">这可能会限制其在需要指关节式 VR 控制器的游戏中的实际可用性。</font><font style="vertical-align: inherit;">如果您正在寻找功能更齐全的 DIY VR 项目 6 自由度跟踪和控制器支持，我们建议您查看</font></font><a href="https://github.com/HadesVR/HadesVR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HadesVR</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无论您是否正在构建 Relativty 或 HadesVR，或者如果您只是想加入我们的社区并闲逛，我们都会邀请您加入</font></font><a href="https://discord.gg/F8GNKjy6RF" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Discord 上的 Relativty 公会</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它是 DIY/开源 VR 社区学习、成长、分享知识和寻求帮助的地方。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开始建立相对论</font></font></h1><a id="user-content-start-building-relativty" class="anchor" aria-label="永久链接：开始建立相对论" href="#start-building-relativty"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/open.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/open.jpg" style="max-width: 100%;"></a> </p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建硬件</font></font></h1><a id="user-content-building-the-hardware" class="anchor" aria-label="永久链接：构建硬件" href="#building-the-hardware"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. 当前建议构建 - 截至 2023 年 5 月</font></font></h2><a id="user-content-1-current-recommended-build---as-of-may-2023" class="anchor" aria-label="永久链接：1.当前推荐构建 - 截至 2023 年 5 月" href="#1-current-recommended-build---as-of-may-2023"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.1 简介</font></font></h3><a id="user-content-11-introduction" class="anchor" aria-label="永久链接：1.1 简介" href="#11-introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是一个更新的构建指南，旨在帮助最近发现 Relativty 的人们。</font><font style="vertical-align: inherit;">它旨在解释如何构建最小的功能产品，并可以根据个人的喜好进一步改进和修改。</font><font style="vertical-align: inherit;">原始构建指南仍然可以在下面找到。</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>        Following this guide to build a Relativty headset assumes basic understanding of electronics and programming Arduino boards.
        A VR headset is a complex system and can be difficult to figure out what is not working if you get stuck with the build.
        If you encounter any issues, join our Discord and ask for help, we are happy to assist.
</code></pre><div class="zeroclipboard-container">
     
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.2 电子产品</font></font></h3><a id="user-content-12-electronics" class="anchor" aria-label="永久链接：1.2 电子" href="#12-electronics"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">需要以下电子元件：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 USB HID 的 ATmega32U4 微控制器 (MCU)，例如</font></font><a href="https://www.amazon.co.uk/diymore-Atmega32U4-Development-Microcontroller-Header/dp/B0BKGSVX2X" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Arduino Pro Micro</font></font></a></li>
<li><font style="vertical-align: inherit;"><a href="https://github.com/LiquidCGS/FastIMU"><font style="vertical-align: inherit;">FastIMU 库</font></a><font style="vertical-align: inherit;">支持的惯性测量单元 (IMU)</font></font><a href="https://github.com/LiquidCGS/FastIMU"><font style="vertical-align: inherit;"></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">小型、高分辨率、高刷新率PC显示屏+驱动板。</font></font><a href="https://github.com/HadesVR/HadesVR/blob/main/docs/Headset.md#displays"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HadesVR 文档的这一部分提供了您可以选择的可能的好选项</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">值得注意的是，这些显示器连接到驱动板，然后驱动板连接到您的计算机。</font><font style="vertical-align: inherit;">如果可以的话，请务必在同一包中购买驱动器板和显示器，以确保它们彼此兼容。</font><font style="vertical-align: inherit;">如果您对显示还有其他疑问，请加入</font></font><a href="https://discord.gg/F8GNKjy6RF" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty's Guild on Discord</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
</ul>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>NOTES on VR DISPLAYS:
- The display does not connect to the Microcontroller, it only connects to the Computer running your VR Apps.
- Technically, any PC display/monitor can be configured as the display used by Relativty. Therefore, you can simply test your build on your PC monitor
first to make sure it works, before you decide to spend a significant amount of money on lenses, display(s) and other parts.
- High performance, small form factor displays are expensive, and often very delicate. Handle them with care! :)
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.2.1 IMU 和 MCU 接线</font></font></h3><a id="user-content-121-wiring-the-imu-and-mcu" class="anchor" aria-label="永久链接：1.2.1 IMU 和 MCU 接线" href="#121-wiring-the-imu-and-mcu"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IMU需要连接到MCU进行供电和通信。</font><font style="vertical-align: inherit;">MCU 通过 USB 连接到您的计算机，将 IMU 读数发送到 SteamVR。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果是 Arduino Pro Micro，您需要连接以下引脚：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>Pro Micro       IMU
VCC         -&gt;  VCC  
GND         -&gt;  GND  
SDA(pin 2)  -&gt;  SDA  
SCL(pin 3)  -&gt;  SCL  
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用不同的 MCU，SDA 和 SCL 引脚可能会映射到不同的引脚号。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">另外，请确保 MCU 的 VCC 与 IMU 的额定工作电压兼容。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向电子元件提供不正确的电压可能会导致其损坏。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MCU 本身只需通过 USB 端口连接到您的计算机。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.2.2 连接显示器</font></font></h3><a id="user-content-122-connecting-the-display" class="anchor" aria-label="永久链接：1.2.2 连接显示器" href="#122-connecting-the-display"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如前所述，任何与个人计算机兼容的显示器都应该能够充当 VR 显示器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用推荐的一个或类似的通过 micro-USB 或其他 USB 标准供电的组件，您可能会遇到主板无法通电的情况。</font><font style="vertical-align: inherit;">这可能是因为您使用的 micro-USB 电缆太长（因此主板无法开机，因为电压下降太多），或者主板无法开机，因为它连接到计算机端的 USB 端口根本无法提供足够的电力。</font><font style="vertical-align: inherit;">在这种情况下，您可能需要尝试计算机或有源 USB 集线器上的不同端口。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.3 机械构建</font></font></h3><a id="user-content-13-mechanical-build" class="anchor" aria-label="永久链接：1.3 机械构建" href="#13-mechanical-build"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机械构建需要以下部件：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">外壳 - Relativty_Mechanical_build 文件夹中提供的 3D 打印模型的 .STL 文件。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">镜头 - 用于构建可 3D 打印的头盔，需要直径 40 毫米/焦距 50 毫米的镜头。</font><font style="vertical-align: inherit;">您通常可以在 Aliexpress 或类似网站上找到这些内容。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表带和面部接口 - 例如 HTC Vive 的替换表带 + 泡沫。</font><font style="vertical-align: inherit;">您通常可以在 Aliexpress 或类似网站上找到这些内容。</font></font></li>
</ul>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/front.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/front.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您无法使用 3D 打印，也可以（而且更简单）仅使用 Android VR 手机壳，并对其进行修改以适合您的屏幕，以便您可以将 IMU 和 MCU 连接到其上。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这种方法的优点是您可以在一个包中获得所有内容，通常包括 IPD 调整。</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/android-vr.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/android-vr.jpg" style="max-width: 100%;"></a> </p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.4 软件设置</font></font></h3><a id="user-content-14-software-setup" class="anchor" aria-label="永久链接：1.4 软件设置" href="#14-software-setup"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.4.1 简介</font></font></h3><a id="user-content-141-introduction" class="anchor" aria-label="永久链接：1.4.1 简介" href="#141-introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativity 取决于 2 个主要软件组件：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Arduino 固件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SteamVR 驱动程序</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由于系统设计为与 SteamVR 配合使用，因此您需要在计算机上安装 Steam 并下载 SteamVR。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.4.2 对 MCU 进行编程</font></font></h3><a id="user-content-142-programming-your-mcu" class="anchor" aria-label="永久链接：1.4.2 对 MCU 进行编程" href="#142-programming-your-mcu"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如前所述，我们建议您使用 Arduino Pro Micro 和 FastIMU 库支持的 IMU。</font><font style="vertical-align: inherit;">FastIMU 是一个很棒的软件包，支持许多常用的 IMU，并附带一个可与 Relativty 配合使用的预先编写的 Arduino 草图。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">首先，您需要安装 Arduino IDE 并通过 USB 连接器将 MCU 连接到计算机。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连接并验证 Arduino IDE 可以与 MCU 配合使用后，从库管理器下载 FastIMU。</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/FastIMU-lib.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/FastIMU-lib.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按照第 1.2.1 节中的建议连接 IMU。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Arduino IDE 中找到 Examples/Fastimu/Caliberated_relativty 草图：</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/FastIMU-sketch.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/FastIMU-sketch.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最后，将其上传到您的 MCU。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FastIMU 还包括一个内置校准工具，可以将校准数据存储在 IMU EEPROM 上。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">校准 IMU 可能有助于解决您在使用 Relativty 时随时间推移可能遇到的传感器漂移问题。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 Caliberated_relativty 草图上传到 IMU 后，您可以打开 Arduino 串行监视器来启动校准序列：</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/FastIMU-calib.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/FastIMU-calib.jpg" style="max-width: 100%;"></a> </p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>  NOTE: you only have to do this once, but make sure to follow the instructions given to you in the serial monitor.
</code></pre><div class="zeroclipboard-container">
     
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.4.3 安装SteamVR驱动</font></font></h3><a id="user-content-143-installing-the-steamvr-driver" class="anchor" aria-label="永久链接：1.4.3 安装 SteamVR 驱动程序" href="#143-installing-the-steamvr-driver"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要安装 Relativty SteamVR 驱动程序：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载</font></font><a href="https://github.com/relativty/Relativty/archive/refs/heads/master.zip"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主存储库</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Relativty-master 中找到 Relativty_Driver\Relativty 文件夹并将其复制到 SteamVR 安装的驱动程序目录中。</font></font></li>
</ul>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/driver-copy.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/driver-copy.jpg" style="max-width: 100%;"></a> </p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.4.4 配置SteamVR驱动程序</font></font></h3><a id="user-content-144-configuring-the-steamvr-driver" class="anchor" aria-label="永久链接：1.4.4 配置 SteamVR 驱动程序" href="#144-configuring-the-steamvr-driver"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复制驱动程序文件后，就可以配置驱动程序以与您的设置和计算机配合使用。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 drivers\Relativty\resources\settings 内，应该有一个名为 default.vrsettings 的文件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是驱动程序的配置文件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您需要更改一些事情。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">配置驱动程序以与 MCU 通信</font></font></h3><a id="user-content-configuring-the-driver-to-talk-to-the-mcu" class="anchor" aria-label="永久链接：配置驱动程序以与 MCU 通信" href="#configuring-the-driver-to-talk-to-the-mcu"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">假设您使用 Arduino Pro Micro 和 FastIMU 库：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Relativty_hmd 段中找到以下值：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hmdPid</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高清视频</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">hmdIMUdmp数据包</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并像这样更改值：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "hmdPid" : 32823,
      "hmdVid": 9025,
      "hmdIMUdmpPackets":  false,
</code></pre><div class="zeroclipboard-container">
  
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用不同的 MCU，则需要计算出 USB PID 和 VID 值。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最简单的方法是通过 USB 将其连接到计算机并检查 Arduino IDE。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在菜单栏中，选择“工具”/“获取主板信息”：</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/board-info.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/board-info.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取 PID 和 VID 值并使用</font></font><a href="https://www.rapidtables.com/convert/number/hex-to-decimal.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">十六进制转换器</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将它们转换为十进制。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后，转换后的值将进入 default.vrsettings 中的 hmdPid 和 hmdVid 值。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">配置显示设置</font></font></h3><a id="user-content-configuring-the-display-settings" class="anchor" aria-label="永久链接：配置显示设置" href="#configuring-the-display-settings"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在让我们看看如何配置驱动程序以与您的显示器配合使用。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">显示的配置变量位于 Relativty_extendedDisplay 段中：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于 VR 视口窗口的原点：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "windowX" : 3440,
      "windowY" : 0,
</code></pre><div class="zeroclipboard-container">
     
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">VR视口的实际尺寸</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "windowWidth" : 1920,
      "windowHeight" : 1080,
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于 VR 视口的渲染分辨率 - 通常应与尺寸相同</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "renderWidth" : 1920,
      "renderHeight" : 1080,
</code></pre><div class="zeroclipboard-container">
     
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">还有一些杂项设置：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "DistortionK1" : 0.4,
      "DistortionK2" : 0.5,
      "ZoomWidth" : 1,
      "ZoomHeight" : 1,
      "EyeGapOffsetPx" : 0,
      "IsDisplayRealDisplay" : true,
      "IsDisplayOnDesktop" : true
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果原点和大小配置不正确，驱动程序将崩溃并且SteamVR将不会显示任何内容！</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因此，我们需要了解并了解 SteamVR 用于显示的坐标系。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows 始终假定连接的显示器之一作为您的主显示器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以在显示设置中验证哪一个是您的主要设备。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以使用鼠标选择每个显示器。</font><font style="vertical-align: inherit;">“将此设为我的主显示器”复选框呈灰色的就是您的主显示器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本指南假设所有显示器的顶部边缘在 WINDOWS 显示设置中对齐（如屏幕截图所示）</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/display-settings.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/display-settings.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因此，在另一台非主显示器上检查相同的内容将使该显示器成为您的主显示器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主显示器的左上角是 SteamVR 显示坐标系的原点。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了能够告诉 SteamVR 在显示器上的何处绘制 VR 视口，您需要确保了解这一事实，从而能够识别视口的正确原点。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">例如，在这样的设置中：</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/display-coordinates.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/display-coordinates.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因为“1”屏幕是主屏幕，而“3”屏幕是VR Display，所以原点（0,0 坐标）位于“1”屏幕的左上角。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该屏幕的分辨率为 3440x1440。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这意味着它占据 X 轴从 0 到 3439 的位置，右侧的下一个屏幕从点 3440 开始。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因此，在这种情况下，正确的 windowX 和 windowY 值为：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "windowX" : 3440,
      "windowY" : 0,
</code></pre><div class="zeroclipboard-container">
   
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果“2”屏幕是 VR 显示器（并且“1”仍然是主屏幕），则正确的值将是：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "windowX" : -1920,
      "windowY" : 0,
</code></pre><div class="zeroclipboard-container">
     
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因为“2”屏幕的坐标占据了原点另一侧的空间。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于 windowWidth、windowHeight、renderWidth、renderHeight，只需设置 VR 显示器的本机分辨率即可。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全部设置完毕后，保存设置文件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在您应该准备好启动 SteamVR。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果一切设置正确，您应该直接进入 VR Holodeck 区域：</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/electronics-assembled.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/electronics-assembled.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您的构建遇到任何问题：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">打开 SteamVR Web 控制台并复制整个日志文件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加入</font></font><a href="https://discord.gg/F8GNKjy6RF" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Discord 上的 Relativty 公会</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，告诉我们您遇到的问题，并在聊天中上传日志文件。</font></font></li>
</ul>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/steamvr-logs.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/steamvr-logs.jpg" style="max-width: 100%;"></a> </p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您的 VR 窗口在点击后消失：</font></font></h3><a id="user-content-if-your-vr-window-disappears-after-clicking-on-it" class="anchor" aria-label="永久链接：如果您的 VR 窗口在点击后消失：" href="#if-your-vr-window-disappears-after-clicking-on-it"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">打开 Windows PowerShell 并将这些命令粘贴到命令行中，然后按 Enter 键：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>get-process vrcompositor | stop-process -Force;cd "C:\Program Files (x86)\Steam\steamapps\common\SteamVR\bin\win64";.\vrcompositor.exe
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">之后 VRCompositor 应保持稳定，因此您只需在每个会话中执行此操作一次。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2. 旧版构建</font></font></h1><a id="user-content-2-legacy-build" class="anchor" aria-label="永久链接：2. 遗留版本" href="#2-legacy-build"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该硬件基于 Relativty 主板，其中包括 Atmel SAM3X8E ARM Cortex-M3 处理器，并使用 MPU-6050 作为 IMU。</font><font style="vertical-align: inherit;">或者，任何支持 ArduinoCore 并连接到 MPU-6050/MPU-9250 的处理器都可以用作 Relativty 的硬件。</font><font style="vertical-align: inherit;">下面解释这两种方法。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建相对论主板</font></font></h2><a id="user-content-building-the-relativty-motherboard" class="anchor" aria-label="永久链接：构建相对论主板" href="#building-the-relativty-motherboard"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PCB 制造。</font></font></h3><a id="user-content-pcb-manufacturing" class="anchor" aria-label="永久链接：PCB 制造。" href="#pcb-manufacturing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们首先从裸露的 PCB 开始。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它可以在</font></font><a href="https://jlcpcb.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">jlcpcb</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">等网站上以 4 美元左右的价格制造和购买。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您需要提供</font></font><code>Relativty_Electronics_build/GerberFiles.zip</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">描述电路板形状的 Gerber 文件夹。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">组装</font></font></h3><a id="user-content-assembling" class="anchor" aria-label="永久链接： 组装" href="#assembling"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将组件焊接到裸露的 PCB 上。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您必须购买两个 BOM 之一中列出的组件，具体取决于可用性：</font></font></p>
<ul dir="auto">
<li><code>Relativty_Electronics_build/Assembly/jlcpcb.com_motherboard_BOM.csv</code></li>
<li><code>Relativty_Electronics_build/Assembly/motherboard_BOM.xlsx</code></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件中描述了这些组件在 PCB 上的放置位置，</font></font><code>Relativty_Electronics_source/motherboard.brd</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该文&ZeroWidthSpace;&ZeroWidthSpace;件可以在 Eagle 中打开。</font></font></p>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/motherboard.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/motherboard.jpg" style="max-width: 100%;"></a> </p>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Arduino</font></font></h4><a id="user-content-using-an-arduino" class="anchor" aria-label="永久链接：使用 Arduino" href="#using-an-arduino"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty 主板的替代方案是使用 Arduino Due 并将其连接到 MPU-6050。</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>5V      -&gt; VCC  
GND     -&gt; GND  
SDA(20) -&gt; SDA  
SCL(21) -&gt; SCL  
PIN 2   -&gt; INT  
</code></pre><div class="zeroclipboard-container">
    
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用带有替代固件的 MPU-9250，则引脚排列为：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>5V      -&gt; VCC  
GND     -&gt; GND  
SDA(20) -&gt; SDA  
SCL(21) -&gt; SCL   
</code></pre><div class="zeroclipboard-container">
   
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后按下Arduino Due 上的</font></font><code>ERASE</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><code>RESET</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按钮，您就可以安装 Relativty 固件。</font></font></p>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 Reltivty 固件</font></font></h4><a id="user-content-installing-the-relativty-firmware" class="anchor" aria-label="永久链接：安装 Relativty 固件" href="#installing-the-relativty-firmware"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="https://github.com/relativty/Relativty/blob/master/ressources/img/cards.jpg"><img src="https://github.com/relativty/Relativty/raw/master/ressources/img/cards.jpg" style="max-width: 100%;"></a> </p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
您现在需要在 Arduino IDE 中安装 Relativty 板。 
</font></font><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为此，请复制 JSON URL：</font></font><a href="https://raw.githubusercontent.com/relativty/Relativty/master/Relativty_Firmware/package_Relativty_board_index.json" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://raw.githubusercontent.com/relativty/Relativty/master/Relativty_Firmware/package_Relativty_board_index.json</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并打开 Arduino IDE</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Arduino 中，单击“文件”，然后单击“首选项”：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用 Relativty PCB，请将 JSON URL 添加到</font></font><code>Additional Boards Manager</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文本框。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">转到</font></font><code>Tools &gt; Board &gt; Board Manager</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，您应该会看到 Relativty Board，单击安装。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重新启动 Arduino IDE，然后在 Tools &gt; Boards 下选择 Relativty。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您现在可以打开</font></font><code>Relativty_Firmware/firmware/firmware.ino</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它并将其上传到您的主板。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用不同的板，例如 Arduino Due：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 的内容安装</font></font><code>Relativty_Firmware\Relativty_board\</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">到您的 Arduino IDE</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用的是 MPU-6050，请使用</font></font><code>Relativty_Firmware/firmware/firmware.ino</code></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用的是 MPU-9250，请使用</font></font><code>Relativty_Firmware\MP9250-HID\MP9250-HID\MP9250-HID.ino</code></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建机械零件</font></font></h1><a id="user-content-building-the-mechanical-parts" class="anchor" aria-label="永久链接：构建机械零件" href="#building-the-mechanical-parts"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">组装 HMD</font></font></h2><a id="user-content-assembling-the-hmd" class="anchor" aria-label="永久链接：组装 HMD" href="#assembling-the-hmd"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3D 打印所需的所有文件都可以在该</font></font><code>Relativty_Mechanical_build</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件夹中找到，组装头盔所需的螺钉列于 中</font></font><code>screws_BOM.xlsx</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们使用了速卖通的零件：</font></font></p>
<ul dir="auto">
<li><a href="https://www.aliexpress.com/item/33058848848.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表带</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">,</font></font></li>
<li><a href="https://www.aliexpress.com/item/4000199486058.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">泡沫</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，</font></font></li>
<li><a href="https://www.aliexpress.com/item/33029909783.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">镜头</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（直径 40 毫米/焦距 50 毫米）。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HMD 的屏幕</font></font></h3><a id="user-content-the-screen-for-the-hmd" class="anchor" aria-label="永久链接：HMD 的屏幕" href="#the-screen-for-the-hmd"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="/relativty/Relativty/blob/master/ressources/img/display.jpg"><img src="/relativty/Relativty/raw/master/ressources/img/display.jpg" style="max-width: 100%;"></a> </p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty 头盔以 120FPS 2K 速度运行双屏，但是，由于 Relativty 的开放性，您可以为其配备任何屏幕。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们的型号可以在 Aliexpress 上找到，但根据供应商的不同，类似的屏幕价格可能从 150 美元到 190 美元不等。</font><font style="vertical-align: inherit;">您必须寻找并可能以合适的价格等待合适的供应商才能以便宜的价格获得显示器（或批量购买）。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是</font></font><a href="https://www.aliexpress.com/item/32975198897.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们使用的模型</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置软件</font></font></h3><a id="user-content-setting-up-the-software" class="anchor" aria-label="永久链接：设置软件" href="#setting-up-the-software"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p align="center" dir="auto"> <a target="_blank" rel="noopener noreferrer" href="/relativty/Relativty/blob/master/ressources/img/front.jpg"><img src="/relativty/Relativty/raw/master/ressources/img/front.jpg" style="max-width: 100%;"></a> </p>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装 SteamVR 的 Relativty 驱动程序</font></font></h4><a id="user-content-installing-relativty-driver-for-steamvr" class="anchor" aria-label="永久链接：安装 SteamVR 的 Relativty 驱动程序" href="#installing-relativty-driver-for-steamvr"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty 驱动程序包含在</font></font><code>Relativty_Driver/Relativty</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件夹内。</font></font></p>
<p dir="auto"><g-emoji class="g-emoji" alias="warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚠️</font></font></g-emoji><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您需要通过编辑 JSON 文件来设置它</font></font><code>Relativty_Driver/Relativty/resources/settings/default.vrsettings</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您不使用 Relativty PCB，则需要更改这些：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>  "hmdPid" : 9,
  "hmdVid": 4617,
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="  &quot;hmdPid&quot; : 9,
  &quot;hmdVid&quot;: 4617," tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这些是 USB HID 设备的唯一供应商和产品标识符 (pid/vid)</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用的是 Arduino Due，则正确的值将是：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>  "hmdPid" : 62,
  "hmdVid" : 9025,
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="  &quot;hmdPid&quot; : 62,
  &quot;hmdVid&quot; : 9025," tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用不同的板，获取正确值的过程如下：</font></font></p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将您的开发板插入</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Arduino IDE 中选择您的开发板，然后单击“工具”/“获取开发板信息”。</font><font style="vertical-align: inherit;">你会看到这样的东西：</font></font></p>
</li>
</ol>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>	BN: Arduino Due (Native USB Port)
	VID: 2341
	PID: 003e
	SN: HIDHB
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="	BN: Arduino Due (Native USB Port)
	VID: 2341
	PID: 003e
	SN: HIDHB" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<ol start="3" dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">记下 VID 和 PID 编号。</font><font style="vertical-align: inherit;">这些是十六进制值。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要将它们应用到配置中，需要将它们转换为 int。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您不确定如何操作，可以使用很多在线转换器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如： https: </font></font><a href="https://www.rapidtables.com/convert/number/hex-to-decimal.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//www.rapidtables.com/convert/number/hex-to-decimal.html</font></font></a></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将您的 hmdPid 和 hmdVid 值更改为转换后的值。</font></font></p>
</li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">接下来，您需要设置显示坐标和分辨率。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">首先，您应该将 HMD 的显示屏设置为扩展桌面的辅助屏幕，</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与主显示屏的右上角对齐。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在配置文件的“Relativty_extendedDisplay”段中，找到并设置这些：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "windowX" : *whatever your primary screen resolution's width is*,
      "windowY" : 0,
      "windowWidth" : *HMD's native resolution width*,
      "windowHeight" : *HMD's native resolution height*,
      "renderWidth" : *HMD's native resolution width*,
      "renderHeight" : *HMD's native resolution height*,
	  
	  And at the bottom of this segment:
	  
      "IsDisplayRealDisplay" : true,
      "IsDisplayOnDesktop" : true
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="      &quot;windowX&quot; : *whatever your primary screen resolution's width is*,
      &quot;windowY&quot; : 0,
      &quot;windowWidth&quot; : *HMD's native resolution width*,
      &quot;windowHeight&quot; : *HMD's native resolution height*,
      &quot;renderWidth&quot; : *HMD's native resolution width*,
      &quot;renderHeight&quot; : *HMD's native resolution height*,
	  
	  And at the bottom of this segment:
	  
      &quot;IsDisplayRealDisplay&quot; : true,
      &quot;IsDisplayOnDesktop&quot; : true" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确保不要删除任何“，”符号，因为这会破坏配置。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">只有配置中的最后一项不应包含“，”符号。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果出于某种原因上述设置对您不起作用，请尝试：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将您的 HMD 显示屏设置为主显示屏的镜像显示屏。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更改配置如下：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "windowX" : 0,
      "windowY" : 0,
      "windowWidth" : *HMD's native resolution width*,
      "windowHeight" : *HMD's native resolution height*,
      "renderWidth" : *HMD's native resolution width*,
      "renderHeight" : *HMD's native resolution height*,
	
      "IsDisplayRealDisplay" : false,
      "IsDisplayOnDesktop" : true
	
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="      &quot;windowX&quot; : 0,
      &quot;windowY&quot; : 0,
      &quot;windowWidth&quot; : *HMD's native resolution width*,
      &quot;windowHeight&quot; : *HMD's native resolution height*,
      &quot;renderWidth&quot; : *HMD's native resolution width*,
      &quot;renderHeight&quot; : *HMD's native resolution height*,
	
      &quot;IsDisplayRealDisplay&quot; : false,
      &quot;IsDisplayOnDesktop&quot; : true
	" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><g-emoji class="g-emoji" alias="warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚠️</font></font></g-emoji><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请注意，这可能会导致 VR 窗口无法捕获键盘/鼠标输入，如果您的游戏需要它，则可能无法玩。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您还可以在配置文件中进行 IPD（瞳距）调整：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在“Relativty_hmd”段中查找并调整：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "IPDmeters" : 0.063,
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="      &quot;IPDmeters&quot; : 0.063," tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您还可以通过更改以下内容来更改镜头畸变校正：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>      "DistortionK1" : 0.4,
      "DistortionK2" : 0.5,
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="      &quot;DistortionK1&quot; : 0.4,
      &quot;DistortionK2&quot; : 0.5," tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您现在可以安装 Relativty 驱动程序：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找到您的</font></font><code>vrpathreg.exe</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">程序，通常位于</font></font><code>C:/Steam/steamapps/common/SteamVR/bin/win64/vrpathreg.exe</code></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后打开 Windows 命令提示符并运行以下命令：
</font></font><code>cd C:/Steam/steamapps/common/SteamVR/bin/win64 vrpathreg.exe</code></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后假设您的</font></font><code>Relativty_Driver/Relativty</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">驱动程序文件夹位于：
</font></font><code>C:/code/Relativty_Driver/Relativty</code></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">跑步</font></font><code>vrpathreg adddriver C:/code/Relativty_Driver/Relativty</code></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Relativty 驱动程序现已安装。</font><font style="vertical-align: inherit;">您可以随时运行以下命令来卸载它：</font></font></p>
<ul dir="auto">
<li><code>vrpathreg removedriver C:/code/Relativty_Driver/Relativty</code></li>
</ul>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置实验性 3D 跟踪</font></font></h4><a id="user-content-setting-up-the-experimental-3d-tracking" class="anchor" aria-label="永久链接：设置实验性 3D 跟踪" href="#setting-up-the-experimental-3d-tracking"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该跟踪仍处于实验阶段，由于使用了 CUDA，只能在 NVIDIA GPU 上运行。</font><font style="vertical-align: inherit;">跟踪仅使用视频输入和经过训练来估计 3D 身体位置的人工神经网络 AI。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这种方法与专用传感器的精度或运动自由度相差甚远，但我们相信该模型可以进行数量级的训练和改进。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您首先需要将网络摄像头连接到计算机并安装Python 3.8.4，然后选择将其添加到路径的选项。</font><font style="vertical-align: inherit;">然后安装</font></font><code>PyTorch</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，您可以通过运行以下命令来完成：</font></font></p>
<p dir="auto"><code>python -m pip install torch===1.6.0 torchvision===0.7.0 -f https://download.pytorch.org/whl/torch_stable.html</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后继续安装</font></font><code>CUDA Toolkit 11.0.</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后，您需要</font></font><code>PYTHONPATH</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在</font></font><a href="https://github.com/relativty/Relativty/releases"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/relativty/Relativty/releases</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载文件夹并将其位置添加</font></font><code>PyPath</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">到</font></font><code>JSON Relativty_Driver/Relativty/resources/settings/default.vrsettings</code></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设置</font></font><code>tracking</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为 1。跟踪现已打开，并且可以通过设置为 0 随时关闭。跟踪也可以根据您的相机进行校准，这可以通过调整</font><font style="vertical-align: inherit;">和 来</font></font><code>tracking</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完成</font><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">对应于当您在给定轴上移动 1 米时，AI 在归一化后计算出的给定轴上的坐标增量。</font></font><code>scalesCoordinateMeter</code><font style="vertical-align: inherit;"></font><code>offsetCoordinate</code><font style="vertical-align: inherit;"></font><code>scalesCoordinateMeter</code><font style="vertical-align: inherit;"></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：</font></font></strong><font style="vertical-align: inherit;"></font><code>hmdIMUdmpPackets</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您计划使用 mpu9250 固件，请不要忘记通过设置</font><font style="vertical-align: inherit;">在驱动程序中切换到它</font></font><code>false</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：
</font></font><code>"hmdIMUdmpPackets":  false,</code></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最后的步骤</font></font></h2><a id="user-content-final-steps" class="anchor" aria-label="永久链接：最后步骤" href="#final-steps"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在一切都已准备就绪，可以开始播放了。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开始使用相对论：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将头盔放在平坦的表面上，前面板朝向地面</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">插入头盔。</font><font style="vertical-align: inherit;">几秒钟后它会自动校准。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您现在可以运行任何 SteamVR 游戏！</font></font></p>
</article></div>
